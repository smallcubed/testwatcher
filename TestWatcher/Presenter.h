//
//  Presenter.h
//  TestWatcher
//
//  Created by Scott Little on 10/06/2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Presenter : NSObject <NSFilePresenter>
@property (copy, readonly) NSURL * presentedItemURL;
@property (strong, readonly) NSOperationQueue * presentedItemOperationQueue;

- (instancetype)initWithURL:(NSURL *)url;
@end

NS_ASSUME_NONNULL_END
