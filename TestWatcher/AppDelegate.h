//
//  AppDelegate.h
//  TestWatcher
//
//  Created by Scott Little on 10/06/2021.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@end

