//
//  AppDelegate.m
//  TestWatcher
//
//  Created by Scott Little on 10/06/2021.
//

#import "AppDelegate.h"
#import "SCFolderWatcher.h"
#import "Presenter.h"


typedef NS_ENUM(NSUInteger, WatchType) {
	WatchTypeFolder,
	WatchTypePresenter,
	WatchTypeDispatchSource,
};

@interface AppDelegate () <SCFolderWatcherDelegate> {
	int _fileDesc;
	dispatch_source_t _source;
}
@property (strong) IBOutlet NSWindow * window;
@property (strong) SCFolderWatcher * watcher;
@property (strong) Presenter * presenter;
@property (assign) WatchType watchType;
@property (strong) NSData * currentBookmarkData;
@property (strong) NSURL * currentURL;
@property (strong) NSString * information;
@property (assign) BOOL fileSecured;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application
	self.currentBookmarkData = [NSUserDefaults.standardUserDefaults objectForKey:@"TestBookmarkData"];
	self.information = @"Initialized…";
	if (self.currentBookmarkData) {
		BOOL isStale = NO;
		NSError * error = nil;
		self.currentURL = [NSURL URLByResolvingBookmarkData:self.currentBookmarkData options:(NSURLBookmarkResolutionWithSecurityScope) relativeToURL:nil bookmarkDataIsStale:&isStale error:&error];
		self.information = @"Initialized with URL…";
	}
	
	[NSNotificationCenter.defaultCenter addObserverForName:@"InfoNote" object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
		self.information = note.object;
	}];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
	[self stopWatchingFile];
}

- (BOOL)applicationSupportsSecureRestorableState:(NSApplication *)app {
	return YES;
}


#pragma mark - Actions

- (IBAction)startWatching:(id)sender {
	[self startWatchingFile];
}

- (IBAction)stopWatching:(id)sender {
	[self stopWatchingFile];
}

- (IBAction)pickType:(NSPopUpButton *)sender {
	self.watchType = sender.selectedTag;
}

- (IBAction)addFolderToWatch:(id)sender {
	NSOpenPanel * myPanel = NSOpenPanel.openPanel;
	myPanel.canChooseDirectories = YES;
	[myPanel beginSheetModalForWindow:self.window completionHandler:^(NSModalResponse result) {
		if (result == NSModalResponseOK) {
			[self stopWatchingFile];
			self.currentURL = myPanel.URL;
			NSError * error = nil;
			self.currentBookmarkData = [self.currentURL bookmarkDataWithOptions:(NSURLBookmarkCreationWithSecurityScope | NSURLBookmarkCreationSecurityScopeAllowOnlyReadAccess) includingResourceValuesForKeys:nil relativeToURL:nil error:&error];
			if (self.currentBookmarkData) {
				[NSUserDefaults.standardUserDefaults setObject:self.currentBookmarkData forKey:@"TestBookmarkData"];
				self.information = @"File Bookmark created";
			}
			else {
				NSLog(@"Error making bookmark data for URL [%@]: %@", self.currentURL, error);
				self.information = @"Error making bookmark from URL";
			}
		}
		else {
			self.information = @"File Load canceled";
		}
	}];	
}


#pragma mark - Watching

- (void)startWatchingFile {
	if (self.currentURL) {
		self.fileSecured = [self.currentURL startAccessingSecurityScopedResource];
	}
	else {
		self.information = @"No file URL found to watch";
		return;
	}
	if (self.currentURL) {
		switch (self.watchType) {
			case WatchTypeFolder:
				[self startFolderWatching];
				break;
				
			case WatchTypePresenter:
				[self startPresenterWatching];
				break;
				
			case WatchTypeDispatchSource:
				[self startDispatchWatching];
				break;
		}
	}
	else {
		self.information = @"Could not make URL";
	}
}

- (void)stopWatchingFile {
	if (!self.currentURL) {
		self.information = @"No URL found to stop watching";
		return;
	}
	switch (self.watchType) {
		case WatchTypeFolder:
			[self stopFolderWatching];
			break;
			
		case WatchTypePresenter:
			[self stopPresenterWatching];
			break;
			
		case WatchTypeDispatchSource:
			[self stopDispatchWatching];
			break;
	}
	if (self.currentURL && self.fileSecured) {
		[self.currentURL stopAccessingSecurityScopedResource];
		self.fileSecured = NO;
	}
}


#pragma mark - Folder Watcher

- (void)startFolderWatching {
	self.watcher = [[SCFolderWatcher alloc] initWithDelegate:self streamFlags:(kFSEventStreamCreateFlagFileEvents | kFSEventStreamCreateFlagWatchRoot | kFSEventStreamCreateFlagUseCFTypes) path:self.currentURL.path];
	self.information = @"Folder Watcher established";
}

- (void)stopFolderWatching {
	if (self.watcher) {
		[self.watcher stopWatching];
		self.information = @"Folder Watcher stopped watching";
	}
	self.watcher = nil;
}

- (void)folderWatcher:(nonnull SCFolderWatcher *)watcher notedChangesToPaths:(nonnull NSArray<NSString *> *)paths { 
	self.information = @"Folder Watcher got change notification";
}



#pragma mark - File Presenter

- (void)startPresenterWatching {
	self.presenter = [[Presenter alloc] initWithURL:self.currentURL];
	[NSFileCoordinator addFilePresenter:self.presenter];
	NSFileCoordinator * coord = [[NSFileCoordinator alloc] initWithFilePresenter:self.presenter];
	NSError * error = nil;
	[coord coordinateReadingItemAtURL:self.currentURL options:NSFileCoordinatorReadingWithoutChanges error:&error byAccessor:^(NSURL * _Nonnull newURL) {
		NSError * readError = nil;
		NSString * contents = [NSString stringWithContentsOfURL:newURL encoding:NSUTF8StringEncoding error:&readError];
		NSLog(@"Read contents %@: error: %@", contents?@"YES":@"NO", error);
		self.information = @"File Presenter established";
	}];
	
}

- (void)stopPresenterWatching {
	if (self.presenter) {
		[NSFileCoordinator removeFilePresenter:self.presenter];
		self.presenter = nil;
		self.information = @"File Presenter stopped watching";
	}
}


#pragma mark - KQueue

- (void)startDispatchWatching {
	dispatch_queue_t myQueue = dispatch_queue_create("Dispatch Source Watcher", DISPATCH_QUEUE_SERIAL);
	_fileDesc = open(self.currentURL.path.UTF8String, O_EVTONLY);
	if (_fileDesc >= 0) {
		_source = dispatch_source_create(DISPATCH_SOURCE_TYPE_VNODE, _fileDesc, (DISPATCH_VNODE_WRITE | DISPATCH_VNODE_RENAME | DISPATCH_VNODE_DELETE), myQueue);
		if (_source != NULL) {
			dispatch_source_set_event_handler(_source, ^{
				self.information = @"Dispatch Source got change notification";
			});
			dispatch_activate(_source);
			self.information = @"Dispatch Source started watching";
		}
		else {
			self.information = @"Could not create source (Dispatch Source)";
		}
	}
	else {
		self.information = @"Could not open() file (Dispatch Source)";
	}
}

- (void)stopDispatchWatching {
	if (_source) {
		dispatch_source_cancel(_source);
		_source = NULL;
		close(_fileDesc);
		_fileDesc = -1;
		self.information = @"Dispatch Source stopped watching";
	}
}


@end
