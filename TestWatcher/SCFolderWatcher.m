//
//  SCFolderWatcher.m
//  SCFoundation
//
//  Created by Scott Morrison on 2021-02-15.
//  Copyright © 2021 SmallCubed Inc. All rights reserved.
//

#import "SCFolderWatcher.h"

@interface SCFolderWatcher() {
    FSEventStreamRef _watchedFolderStreamRef;
}

@property (readwrite, assign) FSEventStreamCreateFlags streamFlags;
@property (readwrite, copy) NSString * watchedPath;
- (void)filesDidChange:(NSArray*)paths;
@end

static void scFSEventsSyncFolder_callback(FSEventStreamRef streamRef, void * clientCallBackInfo, int numEvents, void* eventPaths, const FSEventStreamEventFlags *eventMasks, const uint64_t *eventIDs) {
    NSSet * eventPathSet = [NSSet setWithArray: (__bridge NSArray*)eventPaths];
    SCFolderWatcher * watcher = (__bridge SCFolderWatcher*)clientCallBackInfo;
    [watcher filesDidChange:[eventPathSet allObjects]];
}

static const void* retain_callback(const void *info) {
    return CFBridgingRetain((__bridge id)info);
}

static void release_callback(const void *info) {
    CFBridgingRelease(info);
}


@implementation SCFolderWatcher

- (instancetype)initWithDelegate:(id<SCFolderWatcherDelegate>)delegate path:(NSString *)path {
	return [self initWithDelegate:delegate streamFlags:(kFSEventStreamCreateFlagFileEvents| kFSEventStreamCreateFlagUseCFTypes | kFSEventStreamCreateFlagIgnoreSelf) path:path];
}

- (instancetype)initWithDelegate:(id<SCFolderWatcherDelegate>)delegate streamFlags:(FSEventStreamCreateFlags)streamFlags path:(NSString *)path {
    self = [self init];
    self.delegate = delegate;
    self.watchedPath = path;
	self.streamFlags = streamFlags;
    NSError * error = nil;
    [self startWatchingWithError:&error];
    return self;
}

- (void)dealloc {
    [self stopWatching];
}

- (BOOL)startWatchingWithError:(NSError**)error {
    
    NSString * syncFolderPath = self.watchedPath;
    if (syncFolderPath){
        BOOL isDir = NO;
        if ([NSFileManager.defaultManager fileExistsAtPath:syncFolderPath isDirectory:&isDir] && !isDir){
            
            return NO;
        }
        if ([syncFolderPath isEqualToString:self.watchedPath] && _watchedFolderStreamRef){
           return YES;
        }
    
        Boolean startedOK;
        if (_watchedFolderStreamRef){
            [self stopWatching];
        }
        NSLog(@"starting up Sync Folder Watcher for MT files at Path: %@",syncFolderPath);
        
        // get the reference  for the FSEvent stream if created.
        FSEventStreamContext  context = {0, (__bridge void*)self, retain_callback, release_callback, NULL};
        NSArray *paths = @[syncFolderPath];
        _watchedFolderStreamRef = FSEventStreamCreate(kCFAllocatorDefault,
                                                   (FSEventStreamCallback)&scFSEventsSyncFolder_callback,
                                                   &context,
                                                   (__bridge CFArrayRef)paths,
                                                   kFSEventStreamEventIdSinceNow,
                                                   1,
                                                   self.streamFlags);
        
        
        
        if (NULL == _watchedFolderStreamRef) {
            FSEventStreamInvalidate(_watchedFolderStreamRef);
            FSEventStreamRelease(_watchedFolderStreamRef);
            _watchedFolderStreamRef = NULL;
            return NO;
        }
        
        FSEventStreamScheduleWithRunLoop(_watchedFolderStreamRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        
        startedOK = FSEventStreamStart(_watchedFolderStreamRef);
        if (!startedOK) {
			NSLog(@"Failed to start the SyncFolder Listener\n");
            FSEventStreamInvalidate(_watchedFolderStreamRef);
            FSEventStreamRelease(_watchedFolderStreamRef);
            _watchedFolderStreamRef = NULL;
            return NO;
        }
		NSLog(@"Successfully set up SyncFolder Listener");
        self.watchedPath  = syncFolderPath;
        return YES;
    }
    return NO;
}

- (void)filesDidChange:( NSArray< NSString*> *)paths {
    [self.delegate folderWatcher:self notedChangesToPaths:paths];
}

- (void)stopWatching {
    if (_watchedFolderStreamRef) {
		NSLog(@"stopWatching");
        FSEventStreamStop(_watchedFolderStreamRef);
        FSEventStreamInvalidate(_watchedFolderStreamRef);
        FSEventStreamRelease(_watchedFolderStreamRef);
        _watchedFolderStreamRef= NULL;
    }
    self.watchedPath = nil;
}

@end
