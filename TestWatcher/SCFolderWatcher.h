//
//  SCFolderWatcher.h
//  SCFoundation
//
//  Created by Scott Morrison on 2021-02-15.
//  Copyright © 2021 SmallCubed Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class SCFolderWatcher;

@protocol SCFolderWatcherDelegate <NSObject>
- (void)folderWatcher:(SCFolderWatcher *)watcher notedChangesToPaths:(NSArray<NSString*> *)paths;
@end

@interface SCFolderWatcher : NSObject
@property (weak) id <SCFolderWatcherDelegate> delegate;
@property (readonly) NSString * watchedPath;
@property (readonly) FSEventStreamCreateFlags streamFlags;

- (instancetype)initWithDelegate:(id <SCFolderWatcherDelegate>) delegate path:(NSString *)path;
- (instancetype)initWithDelegate:(id<SCFolderWatcherDelegate>)delegate streamFlags:(FSEventStreamCreateFlags)streamFlags path:(NSString *)path;
- (BOOL)startWatchingWithError:(NSError**)error;  // note, right now does not pass back error, check reutrn value for succes.
- (void)stopWatching;
@end

NS_ASSUME_NONNULL_END
