//
//  Presenter.m
//  TestWatcher
//
//  Created by Scott Little on 10/06/2021.
//

#import "Presenter.h"


@interface Presenter ()
@property (copy, readwrite) NSURL * presentedItemURL;
@property (strong, readwrite) NSOperationQueue * presentedItemOperationQueue;
@end


@implementation Presenter


- (instancetype)initWithURL:(NSURL *)url {
	self = [super init];
	if (self) {
		self.presentedItemURL = url;
		self.presentedItemOperationQueue = [NSOperationQueue new];
		self.presentedItemOperationQueue.name = @"Presenter Queue";
		self.presentedItemOperationQueue.maxConcurrentOperationCount = 1;
		self.presentedItemOperationQueue.qualityOfService = NSQualityOfServiceBackground;
	}
	return self;
}

- (void)presentedItemDidGainVersion:(NSFileVersion *)version {
	[NSNotificationCenter.defaultCenter postNotificationName:@"InfoNote" object:@"File Presenter got change notification"];
}

@end
